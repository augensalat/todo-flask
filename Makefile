.venv:  ## create virtual environment if venv is not present
	python3 -m venv .venv

requirements.txt: requirements.in | .venv		## generate requirements for release
	.venv/bin/pip-compile -o requirements.txt requirements.in

install: requirements.txt ## creates a development environment, install deps
	.venv/bin/pip-sync requirements.txt

server: install
	FLASK_APP=app.py FLASK_ENV=development .venv/bin/flask run
