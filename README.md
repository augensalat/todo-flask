# ToDo App - built with Flask

## Initial Project Setup

```shell
# Linux shell
git clone ...
cd todo-flask
python3 -m venv .venv
. .venv/bin/activate
python3 -m pip install --upgrade pip
pip install pip-tools
cat <<ENV >.env
FLASK_APP=app.py
FLASK_ENV=development
ENV
```

## Run the development server

```shell
make server
```
